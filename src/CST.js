

export const CST = {
    SCENES: {
        LOAD: "LOAD",
        MENU: "MENUS",
        WORLD: "WORLD",
        GAME_UI: "GAME_UI"
    },
    IMAGE: {
        CLOUDS_BG: "bg-clouds.png",
        MOUNT_BG: "bg-mountains.png",
        TREES_BG: "bg-trees.png",
        OPTIONS: "options.png",
        RESUME: "resume_game.png",
        NEW: "new_game.png",
        TITLE: "homestead_title.png"
    },
    SPRITE: {
        ROBIN: {
            KEY: 'robin',
            SPRITES: 'robin_fly.png',
            WIDTH: 32,
            HEIGHT: 32
        },
        TREE: {
            KEY: 'tree',
            SPRITES: 'tree.png',
            WIDTH: 32,
            HEIGHT: 32
        },
        GRASS: {
            KEY: 'grass',
            SPRITES: 'grass.png',
            WIDTH: 32,
            HEIGHT: 32
        },
        WATER_GRASS: {
            KEY: 'water_grass',
            SPRITES: 'watergrass.png',
            WIDTH: 32,
            HEIGHT: 32
        },
        BRIDGES: {
            KEY: 'bridges',
            SPRITES: 'bridges.png',
            WIDTH: 32,
            HEIGHT: 32
        },
        UI_HEART: {
            KEY: 'ui-heart',
            SPRITES: 'heartsprite.png',
            WIDTH: 16,
            HEIGHT: 16
        },
        PLAIN_ARROW: {
            KEY: 'plain_arrow',
            SPRITES: 'arrow-knocked.png',
            WIDTH: 27,
            HEIGHT: 12
        },
        ITEM_BAR: {
            KEY: 'itembar',
            SPRITES: 'itembar.png',
            WIDTH: 44,
            HEIGHT: 44
        }
    },
    ATLAS_SPRITES: {
        HERO: {
            KEY: 'hero',
            SPRITES: 'hero.png',
            ATLAS: 'hero.json'
        },
        HERO_ALT: {
            KEY: 'hero_alt',
            SPRITES: 'hero_alt.png',
            ATLAS: 'hero_alt.json'
        },
        BAT: {
            KEY: 'bat',
            SPRITES: 'bat.png',
            ATLAS: 'bat.json'
        },
        CHESTS: {
            KEY: 'treasure_chest',
            SPRITES: 'selectedChests.png',
            ATLAS: 'selectedChests.json'
        }
    },
    AUDIO: {
        TITLE_MUSIC: "Carefree.mp3",
        WORLD_MUSIC: "Fretless.mp3"
    }
}

